class Bottles
  FIRST = 99
  LAST = 0

  VERSES = {
    0 => "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, #{FIRST} bottles of beer on the wall.\n",
    1 => "1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n",
    2 => "2 bottles of beer on the wall, 2 bottles of beer.\nTake one down and pass it around, 1 bottle of beer on the wall.\n",
  }

  def song
    verses(FIRST, LAST)
  end

  def verses(first, last)
    first.downto(last).map { |bottles| verse(bottles)}.join("\n")
  end

  def verse(bottles)
    VERSES.fetch(bottles) do |bottles|
      "#{bottles} bottles of beer on the wall, #{bottles} bottles of beer.\nTake one down and pass it around, #{bottles - 1} bottles of beer on the wall.\n"
    end
  end
end
